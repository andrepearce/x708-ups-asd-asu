# RPi X708 UPS Auto-shutdown

An auto-shutdown script for a RaspberryPi with an X708 v1.2 UPS HAT. Documentation specific to the X708 v1.2 UPS HAT can be found on the [Geekworm wiki](https://wiki.geekworm.com/X708).

This project comprises of a python script (`x708asd.py`), which serves as the auto-shutdown script, python tests (`test_x708asd.py`) and a service file for the script (`./systemd`). In the event power is loss from the X708, the script will initiate a shutdown scheduled for 5 minutes from the time the power loss was detected. If power is restored before the shutdown timer has lapsed, the timer will then be cancelled.

It is also worth mentioning that auto-startup can be enabled once power is restored on the X708 by shorting the 'AON' pin on the board.

## Pre-requisites

It is expected that before using the scripts in this project that you have met the following pre-requisites:

- RaspberryPi with the RPi.GPIO python package installed;
- Geekworm X708 v1.2 UPS HAT installed on the RaspberryPi;
- Geekworm X708 software installed, as per [X708-Software](https://wiki.geekworm.com/X708-Software);
- Clone this repository onto the RaspberryPi.

## Usage

This section explains the steps for executing `x708asd.py` in a standalone setting. To confgure this script to run automatically on startup, please refer to [Systemd Service](#systemd-service).

The following steps explain how to execute the `x708asd.py` script:

1. Navigate to the location on the RaspberryPi where this repository has been cloned. For example:

   ```sh
   cd ~/rpi-x708-ups-asd
   ```

2. Setup a python virtual environment, activate and install requirements.

   ```sh
   python3 -m venv .venv
   source .venv/bin/activate
   pip install -r requirements.txt
   ```

3. Run the script to indefinitely monitor for a power loss event.

   ```sh
   python x708asd.py
   ```

   To enable debug logs, run the script with the `--debug` flag.

   ```sh
   python x708asd.py --debug
   ```

## Systemd Service

A Systemd service file for the script has been included in this project. This can be used to configure the script to startup and run automatically when the RaspberryPi boots. The service file can be found at `./systemd/x708asd.service`. The service file assumes the `x708asd.py` script has been placed on the RaspberryPi at `/opt/x708/x708asd.py`. If you choose to place the `x708asd.py` file in a different location remember to update the `ExecStart` property in `x708asd.service`.

The below steps describe the process to install the script on the RaspberryPi and use Systemd to manage it.

1. Navigate to the location on the RaspberryPi where this repository has been cloned. For example:

   ```sh
   cd ~/rpi-x708-ups-asd
   ```

2. Install the requirements with the root user (the user that Systemd will use to run the script).

   ```sh
   sudo pip install -r requirements.txt
   ```

3. Copy files and setup permissions.

   ```sh
   sudo mkdir /opt/x708
   sudo cp ./x708asd.py /opt/x708/x708asd.py
   sudo cp ./systemd/x708asd.service /etc/systemd/system/x708asd.service
   sudo chmod 644 /etc/systemd/system/x708asd.service
   ```

4. Reload, enable on startup and run the service.

   ```sh
   sudo systemctl daemon-reload
   sudo systemctl enable x708asd.service
   sudo systemctl start x708asd.service
   ```

5. Verify the service has started.

   ```sh
   sudo systemctl status x708asd.service
   ```

   Note: you can view the log output of the service using the below command.

   ```sh
   journalctl -f -u x708asd.service
   ```

## Testing

This project includes some test cases for the `x708asd.py` script. These tests are expected to be run using `pytest`. Follow the steps below to run the tests for yourself.

1. Navigate to the location on the RaspberryPi where this repository has been cloned. For example:

   ```sh
   cd ~/rpi-x708-ups-asd
   ```

2. Setup a python virtual environment, activate and install requirements.

   ```sh
   python3 -m venv .venv
   source .venv/bin/activate
   pip install -r requirements.txt
   ```

3. Run the tests.

   ```sh
   pytest
   ```

## Contributing

Always happy for any help, feel free to raise any issues you find and merge requests.
